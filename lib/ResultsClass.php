<?php
/**
 * Created by PhpStorm.
 * User: klapek
 * Date: 2016-06-12
 * Time: 15:30
 */

namespace RoiCalculator;

require_once(dirname(__FILE__) . '/DbClass.php' );
require_once(dirname(__FILE__) . '/../../../../wp-load.php' );

class ResultsClass extends DbClass {

    public function getResult($id) {
        global $wpdb;

        $result = $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . $this->resultsTable . " WHERE id = '$id'");

        return $result;
    }

    public function getAllResults() {
        global $wpdb;

        $result = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . $this->resultsTable);

        return $result;
    }

    public function createResult($data) {
        global $wpdb;

        $status = $wpdb->insert($wpdb->prefix . $this->resultsTable,
            array(
                'full_name' => $data->full_name,
                'email'     => $data->email,
                'company'   => $data->company,
                'calculations'  => $data->calculations
            ),
            array(
                '%s',
                '%s',
                '%s'
            )
        );

        if($status) {
            return array(
                'status'    => 'ok',
                'error'    => null
            );
        } else {
            return false;
        }
    }

    public function updateResult($id, $data) {
        global $wpdb;

        $result = $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . $this->resultsTable . " WHERE id = '$id'");

        if($result) {
            $status = $wpdb->update($wpdb->prefix . $this->resultsTable,
                array(
                    'full_name' => $data->full_name,
                    'email'     => $data->email,
                    'company'   => $data->company,
                    'calculations'  => $data->calculations
                ),
                array(
                    'id'    => $id
                )
            );

            if($status && $status > 0) {
                return array(
                    'status'    => 'ok',
                    'error'    => null
                );
            } else {
                return false;
            }
        } else {
           return false;
        }
    }

    public function removeResult($id) {
        global $wpdb;

        if($this->getResult($id)) {
            $result = $wpdb->delete( $wpdb->prefix . $this->resultsTable,
                array(
                    'id'    =>  $id
                )
            );

            if($result) {
                return array(
                    'type'    =>  'ok',
                    'error'   => null
                );
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
} 