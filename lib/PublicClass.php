<?php
/**
 * Created by PhpStorm.
 * User: klapek
 * Date: 2016-05-22
 * Time: 14:42
 */

namespace RoiCalculator;


class PublicClass {

    static function registerScripts() {
        if(!wp_script_is('angular', 'enqueued')) {
            wp_register_script('angular', CALCULATOR_ROI_PLUGIN_DIR_URL . 'app/build/js/libs/angular/angular.min.js', array(), false, true);
            wp_enqueue_script('angular');
        }

        if(!wp_script_is('angular-animate', 'enqueued')) {
            wp_register_script('angular-animate', CALCULATOR_ROI_PLUGIN_DIR_URL . 'app/build/js/libs/angular-animate/angular-animate.min.js', array(), false, true);
            wp_enqueue_script('angular-animate');
        }

        if(!wp_script_is('angular-bootstrap', 'enqueued')) {
            wp_register_script('angular-bootstrap', CALCULATOR_ROI_PLUGIN_DIR_URL . 'app/build/js/libs/angular-bootstrap/ui-bootstrap.min.js', array(), false, true);
            wp_enqueue_script('angular-bootstrap');
        }

        if(!wp_script_is('angular-bootstrap-tpls', 'enqueued')) {
            wp_register_script('angular-bootstrap-tpls', CALCULATOR_ROI_PLUGIN_DIR_URL . 'app/build/js/libs/angular-bootstrap/ui-bootstrap-tpls.min.js', array(), false, true);
            wp_enqueue_script('angular-bootstrap-tpls');
        }

        if(!wp_script_is('angular-messages', 'enqueued')) {
            wp_register_script('angular-messages', CALCULATOR_ROI_PLUGIN_DIR_URL . 'app/build/js/libs/angular-messages/angular-messages.min.js', array(), false, true);
            wp_enqueue_script('angular-messages');
        }

        if(!wp_script_is('angular-touch', 'enqueued')) {
            wp_register_script('angular-touch', CALCULATOR_ROI_PLUGIN_DIR_URL . 'app/build/js/libs/angular-touch/angular-touch.min.js', array(), false, true);
            wp_enqueue_script('angular-touch');
        }

        if(!wp_script_is('angular-ui-router', 'enqueued')) {
            wp_register_script('angular-ui-router', CALCULATOR_ROI_PLUGIN_DIR_URL . 'app/build/js/libs/angular-ui-router/release/angular-ui-router.min.js', array(), false, true);
            wp_enqueue_script('angular-ui-router');
        }

        if(!wp_script_is('chart.js', 'enqueued')) {
            wp_register_script('chart.js', CALCULATOR_ROI_PLUGIN_DIR_URL . 'app/build/js/libs/Chart.js/dist/Chart.js', array(), false, true);
            wp_enqueue_script('chart.js');
        }

        if(!wp_script_is('tc-angular-chartjs', 'enqueued')) {
            wp_register_script('tc-angular-chartjs', CALCULATOR_ROI_PLUGIN_DIR_URL . 'app/build/js/libs/tc-angular-chartjs/dist/tc-angular-chartjs.min.js', array(), false, true);
            wp_enqueue_script('tc-angular-chartjs');
        }


        wp_register_script('calculator_app', CALCULATOR_ROI_PLUGIN_DIR_URL . 'app/build/js/app.js', array(), false, true);
        wp_enqueue_script('calculator_app');
    }

    public function registerStyles() {
        wp_register_style('calculator_styles', CALCULATOR_ROI_PLUGIN_DIR_URL . 'app/build/css/calculator-roi.css');

        wp_enqueue_style('calculator_styles');
    }
} 