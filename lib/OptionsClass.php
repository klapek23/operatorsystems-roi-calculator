<?php
/**
 * Created by PhpStorm.
 * User: klapek
 * Date: 2016-06-12
 * Time: 15:30
 */

namespace RoiCalculator;

require_once(dirname(__FILE__) . '/DbClass.php' );
require_once(dirname(__FILE__) . '/../../../../wp-load.php' );

class OptionsClass extends DbClass {

    public function getAllOptions() {
        global $wpdb;

        $result = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . $this->optionsTable, ARRAY_A);

        return $result;
    }

    public function getOption($id) {

    }

    public function getOptionByName($option_name) {
        global $wpdb;

        $option = $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . $this->optionsTable . " WHERE option_name = '$option_name'");

        return $option;
    }

    public function createOption($option_name, $option_value) {
        global $wpdb;

        $status = $wpdb->insert($wpdb->prefix . $this->optionsTable,
            array(
                'option_name'      => $option_name,
                'option_value'     => $option_value
            )
        );

        if($status) {
            return array(
                'success'   => true,
                'error'    => null
            );
        } else {
            return false;
        }
    }

    public function updateOption($option_name, $option_value) {
        global $wpdb;

        $option = $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . $this->optionsTable . " WHERE option_name = '$option_name'");

        if(!$option) {
            return false;
        } else {
            $status = $wpdb->update($wpdb->prefix . $this->optionsTable,
                array(
                    'option_value' => $option_value
                ),
                array(
                    'option_name' => $option_name
                )
            );

            if($status && $status > 0) {
                return array(
                    'success' => true,
                    'message' => __('Option updated')
                );
            } else {
                return false;
            }
        }
    }
} 