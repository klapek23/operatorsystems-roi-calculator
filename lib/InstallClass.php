<?php
/**
 * Created by PhpStorm.
 * User: klapek
 * Date: 2016-05-22
 * Time: 12:00
 */

namespace RoiCalculator;

class InstallClass {
    private $resultsTable = 'calculator_roi_results';
    private $optionsTable = 'calculator_roi_options';

    public function install() {
        global $wpdb;
        $charset_collate = $wpdb->get_charset_collate();

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

        //check if table not exist
        if($wpdb->get_var("SHOW TABLES LIKE $wpdb->prefix . $this->resultsTable") != $wpdb->prefix . $this->resultsTable) {
            $createTable = "CREATE TABLE " . $wpdb->prefix . $this->resultsTable . " (
				ID int(11) NOT NULL AUTO_INCREMENT,
			    full_name varchar(255) NOT NULL,
			    email varchar(255) NOT NULL,
			    company varchar(255) NOT NULL,
			    calculations text,
				date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
			    UNIQUE KEY ID (ID)
            ) $charset_collate;";

            dbDelta($createTable);
        }

        //check if table not exist
        if($wpdb->get_var("SHOW TABLES LIKE $wpdb->prefix . $this->optionsTable") != $wpdb->prefix . $this->optionsTable) {
            $createTable = "CREATE TABLE " . $wpdb->prefix . $this->optionsTable . " (
				ID int(11) NOT NULL AUTO_INCREMENT,
			    option_name varchar(255) NOT NULL,
			    option_value text,
				date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
			    UNIQUE KEY ID (ID)
			) $charset_collate;";

            dbDelta($createTable);
        }
    }

    public function uninstall() {
        global $wpdb;

        $wpdb->query("DROP TABLE IF EXISTS $wpdb->prefix . $this->resultsTable");
        $wpdb->query("DROP TABLE IF EXISTS $wpdb->prefix . $this->optionsTable");
    }
} 