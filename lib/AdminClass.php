<?php
/**
 * Created by PhpStorm.
 * User: klapek
 * Date: 2016-05-22
 * Time: 12:05
 */

namespace RoiCalculator;

require_once(dirname(__FILE__) . '/OptionsClass.php');
require_once(dirname(__FILE__) . '/ResultsClass.php');


class AdminClass {

    public function enqueueAdminScripts() {
        if(!wp_script_is('angular', 'enqueued')) {
            wp_register_script('angular', CALCULATOR_ROI_PLUGIN_DIR_URL . 'app/build/js/libs/angular/angular.min.js', array(), false, true);
            wp_enqueue_script('angular');
        }

        if(!wp_script_is('angular-animate', 'enqueued')) {
            wp_register_script('angular-animate', CALCULATOR_ROI_PLUGIN_DIR_URL . 'app/build/admin/js/libs/angular-animate/angular-animate.min.js', array(), false, true);
            wp_enqueue_script('angular-animate');
        }

        if(!wp_script_is('angular-bootstrap', 'enqueued')) {
            wp_register_script('angular-bootstrap', CALCULATOR_ROI_PLUGIN_DIR_URL . 'app/build/admin/js/libs/angular-bootstrap/ui-bootstrap.min.js', array(), false, true);
            wp_enqueue_script('angular-bootstrap');
        }

        if(!wp_script_is('angular-bootstrap-tpls', 'enqueued')) {
            wp_register_script('angular-bootstrap-tpls', CALCULATOR_ROI_PLUGIN_DIR_URL . 'app/build/admin/js/libs/angular-bootstrap/ui-bootstrap-tpls.min.js', array(), false, true);
            wp_enqueue_script('angular-bootstrap-tpls');
        }

        if(!wp_script_is('angular-messages', 'enqueued')) {
            wp_register_script('angular-messages', CALCULATOR_ROI_PLUGIN_DIR_URL . 'app/build/admin/js/libs/angular-messages/angular-messages.min.js', array(), false, true);
            wp_enqueue_script('angular-messages');
        }
    }

    //add page to menu
    public function addMenuItem() {
        add_menu_page( 'Calculator ROI', 'Calculator ROI', 'level_1', 'calculator-roi', array($this, 'addAdminPage'), '', 22);
        add_submenu_page ( 'calculator-roi', 'Options', 'Options', 'level_1', 'calculator-roi-options', array($this, 'addOptionsSubpage') );
    }

    public function addAdminPage() {
        $resultsAdapter = new \RoiCalculator\ResultsClass();
        $results = $resultsAdapter->getAllResults();
        arsort($results);

        $this->enqueueAdminScripts();

        wp_register_script('calculator-admin-app', CALCULATOR_ROI_PLUGIN_DIR_URL . 'app/build/admin/js/app.js', array(
            'angular', 'angular-animate', 'angular-bootstrap', 'angular-bootstrap-tpls', 'angular-messages'
        ), false, true);
        wp_enqueue_script('calculator-admin-app');
        ?>

        <!-- PAGE TEMPLATE -->
        <link rel="stylesheet" type="text/css" href="/wp-content/plugins/calculator-roi/app/build/css/calculator-roi.css">

        <div class="wrap calculator-roi-admin" ng-app="CalculatorROIAdmin" ng-controller="ResultsController" ng-init='init(<?php echo json_encode($results); ?>)'>
            <div class="calculator-loader" ng-if="showLoader">
                <img src="/wp-content/plugins/calculator-roi/app/build/img/loader-icon.png" alt="loaderek">
            </div>

            <div id="icon-themes" class="icon32"><br /></div>
            <h2>Calculator results page</h2>

            <table style="width: 100%; border-collapse: collapse;">
                <thead>
                    <tr>
                        <th style="vertical-align: top; border: 1px solid #000; padding: 4px 8px;">ID</th>
                        <th style="vertical-align: top; border: 1px solid #000; padding: 4px 8px;">Full name</th>
                        <th style="vertical-align: top; border: 1px solid #000; padding: 4px 8px;">Email</th>
                        <th style="vertical-align: top; border: 1px solid #000; padding: 4px 8px;">Company</th>
                        <th style="vertical-align: top; border: 1px solid #000; padding: 4px 8px;">Date add</th>
                        <th style="vertical-align: top; border: 1px solid #000; padding: 4px 8px; text-align: right">Calculations</th>
                    </tr>
                </thead>
                <tbody>
                <?php $i = 1; ?>
                    <?php foreach($results as $key => $result): ?>
                        <?php $result->calculations = unserialize($result->calculations); ?>
                        <tr>
                            <td style="vertical-align: top; border: 1px solid #000; padding: 4px 8px;"><?php echo $i; ?></td>
                            <td style="vertical-align: top; border: 1px solid #000; padding: 4px 8px;"><?php echo $result->full_name; ?></td>
                            <td style="vertical-align: top; border: 1px solid #000; padding: 4px 8px;"><?php echo $result->email; ?></td>
                            <td style="vertical-align: top; border: 1px solid #000; padding: 4px 8px;"><?php echo $result->company; ?></td>
                            <td style="vertical-align: top; border: 1px solid #000; padding: 4px 8px;"><?php echo $result->date; ?></td>
                            <td style="vertical-align: top; border: 1px solid #000; padding: 4px 8px; text-align: right">
                                <button type="button" class="btn btn-info" ng-click='openModal(<?php echo json_encode($result); ?>)'>Show calculations</button>
                                <button type="button" class="btn btn-danger" ng-click='removeItem(<?php echo $result->ID; ?>)'>Remove</button>
                            </td>
                        </tr>
                        <?php $i++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <?php
    }


    public function addOptionsSubpage() {
        $optionsAdapter = new \RoiCalculator\OptionsClass();
        $defaults = $optionsAdapter->getOptionByName('default_values');
        $defaults = unserialize($defaults->option_value);

        $this->enqueueAdminScripts();

        wp_register_script('calculator-admin-app', CALCULATOR_ROI_PLUGIN_DIR_URL . 'app/build/admin/js/app.js', array(
            'angular', 'angular-animate', 'angular-bootstrap', 'angular-bootstrap-tpls', 'angular-messages'
        ), false, true);
        wp_enqueue_script('calculator-admin-app');
        ?>

        <!-- PAGE TEMPLATE -->
        <link rel="stylesheet" type="text/css" href="/wp-content/plugins/calculator-roi/app/build/css/calculator-roi.css">

        <div class="wrap calculator-roi-admin" ng-app="CalculatorROIAdmin" ng-controller="AdminController" ng-init='init(<?php echo json_encode($defaults); ?>)'>
            <div id="icon-themes" class="icon32"><br /></div>
            <h2>Calculator settings page</h2>

            <form name="calculatorRoiOptionsForm" id="calculator-roi-options-form" ng-submit="submitForm(calculatorRoiOptionsForm.$valid, $event)" novalidate>
                <div class="calculator-loader" ng-if="showLoader">
                    <img src="/wp-content/plugins/calculator-roi/app/build/img/loader-icon.png" alt="loaderek">
                </div>

                <div class="alert alert-success" role="alert" ng-if="formSaveResults.success">
                    <p>{{formSaveResults.message}}</p>
                </div>

                <fieldset>
                    <h3>Form constants</h3>
                    <div class="form-field" validate-field>
                        <label for="C16"># of weeks / year the identical machines are in operation</label>
                        <div class="input-with-unit">
                            <input type="number" name="C16" id="C16" ng-model="calculator.options.fields.C16" required>

                            <div ng-messages="calculatorRoiOptionsForm.C16.$error" role="alert">
                                <p class="help-block error-text" ng-message="required">This field is required.</p>
                                <p class="help-block error-text" ng-message="number">This field has to be a number.</p>
                            </div>
                        </div>

                        <div class="form-field" validate-field>
                            <label for="C18"># of days / week the identical machines are in operation</label>
                            <div class="input-with-unit">
                                <input type="number" name="C18" id="C18" ng-model="calculator.options.fields.C18" required>

                                <div ng-messages="calculatorRoiOptionsForm.C18.$error" role="alert">
                                    <p class="help-block error-text" ng-message="required">This field is required.</p>
                                    <p class="help-block error-text" ng-message="number">This field has to be a number.</p>
                                </div>
                            </div>
                        </div>

                        <div class="form-field" validate-field>
                            <label for="C20"># of shifts / day the identical machines are in operation</label>
                            <div class="input-with-unit">
                                <input type="number" name="C20" id="C20" ng-model="calculator.options.fields.C20" required>

                                <div ng-messages="calculatorRoiOptionsForm.C20.$error" role="alert">
                                    <p class="help-block error-text" ng-message="required">This field is required.</p>
                                    <p class="help-block error-text" ng-message="number">This field has to be a number.</p>
                                </div>
                            </div>
                        </div>

                        <div class="form-field" validate-field>
                            <label for="C22"># of hours / shift the identical machines are in operation (including all breaks)</label>
                            <div class="input-with-unit">
                                <input type="number" name="C22" id="C22" ng-model="calculator.options.fields.C22" required>

                                <div ng-messages="calculatorRoiOptionsForm.C22.$error" role="alert">
                                    <p class="help-block error-text" ng-message="required">This field is required.</p>
                                    <p class="help-block error-text" ng-message="number">This field has to be a number.</p>
                                </div>
                            </div>
                        </div>

                        <div class="form-field" validate-field>
                            <label for="M19">Basic costs (foundation) €</label>
                            <div class="input-with-unit">
                                <input type="number" name="M19" id="M19" ng-model="calculator.options.fields.M19" required>

                                <div ng-messages="calculatorRoiOptionsForm.M19.$error" role="alert">
                                    <p class="help-block error-text" ng-message="required">This field is required.</p>
                                    <p class="help-block error-text" ng-message="number">This field has to be a number.</p>
                                </div>
                            </div>
                        </div>

                        <div class="form-field" validate-field>
                            <label for="M20">Implementations € / per</label>
                            <div class="input-with-unit">
                                <input type="number" name="M20" id="M20" ng-model="calculator.options.fields.M20" required>

                                <div ng-messages="calculatorRoiOptionsForm.M19.$error" role="alert">
                                    <p class="help-block error-text" ng-message="required">This field is required.</p>
                                    <p class="help-block error-text" ng-message="number">This field has to be a number.</p>
                                </div>
                            </div>
                        </div>

                        <div class="form-field" validate-field>
                            <label for="M21">License costs per machine €</label>
                            <div class="input-with-unit">
                                <input type="number" name="M21" id="M21" ng-model="calculator.options.fields.M21" required>

                                <div ng-messages="calculatorRoiOptionsForm.M21.$error" role="alert">
                                    <p class="help-block error-text" ng-message="required">This field is required.</p>
                                    <p class="help-block error-text" ng-message="number">This field has to be a number.</p>
                                </div>
                            </div>
                        </div>

                        <div class="form-field" validate-field>
                            <label for="M25">Annual maintenance costs (in %)</label>
                            <div class="input-with-unit">
                                <input type="number" name="M25" id="M25" ng-model="calculator.options.fields.M25" required min="0" max="100">

                                <div ng-messages="calculatorRoiOptionsForm.M25.$error" role="alert">
                                    <p class="help-block error-text" ng-message="required">This field is required.</p>
                                    <p class="help-block error-text" ng-message="number">This field has to be a number.</p>
                                    <p class="help-block error-text" ng-message="min">Value cannot be lower than 0%.</p>
                                    <p class="help-block error-text" ng-message="max">Value cannot be greater than 100%.</p>
                                </div>
                            </div>
                        </div>

                        <div class="form-field" validate-field>
                            <label for="M28">Machine rolout 1 hour consultancy cost</label>
                            <div class="input-with-unit">
                                <input type="number" name="M28" id="M28" ng-model="calculator.options.fields.M28">

                                <div ng-messages="calculatorRoiOptionsForm.M25.$error" role="alert">
                                    <p class="help-block error-text" ng-message="required">This field is required.</p>
                                    <p class="help-block error-text" ng-message="number">This field has to be a number.</p>
                                </div>
                            </div>
                        </div>

                        <div class="form-field" validate-field>
                            <label for="M29">Machine rolout asistance hour qty</label>
                            <div class="input-with-unit">
                                <input type="number" name="M29" id="M29" ng-model="calculator.options.fields.M29">

                                <div ng-messages="calculatorRoiOptionsForm.M25.$error" role="alert">
                                    <p class="help-block error-text" ng-message="required">This field is required.</p>
                                    <p class="help-block error-text" ng-message="number">This field has to be a number.</p>
                                </div>
                            </div>
                        </div>

                        <div class="form-field" validate-field>
                            <label for="M40">Meal Break (pre-defined, as 1@30 min.)</label>
                            <div class="input-with-unit">
                                <input type="number" name="M40" id="M40" ng-model="calculator.options.fields.M40" required>

                                <div ng-messages="calculatorRoiOptionsForm.M40.$error" role="alert">
                                    <p class="help-block error-text" ng-message="required">This field is required.</p>
                                    <p class="help-block error-text" ng-message="number">This field has to be a number.</p>
                                </div>
                            </div>
                        </div>

                        <div class="form-field" validate-field>
                            <label for="M41">Short Breaks (pre-defined, as 2@15 min.)</label>
                            <div class="input-with-unit">
                                <input type="number" name="M41" id="M41" ng-model="calculator.options.fields.M41" required>

                                <div ng-messages="calculatorRoiOptionsForm.M41.$error" role="alert">
                                    <p class="help-block error-text" ng-message="required">This field is required.</p>
                                    <p class="help-block error-text" ng-message="number">This field has to be a number.</p>
                                </div>
                            </div>
                        </div>
                </fieldset>

                <fieldset>
                    <h3>World Class OEE</h3>
                    <div class="form-field" validate-field>
                        <label for="M38">Availability</label>
                        <div class="input-with-unit">
                            <input type="number" name="M38" id="M38" ng-model="calculator.options.fields.M38" required>

                            <div ng-messages="calculatorRoiOptionsForm.M38.$error" role="alert">
                                <p class="help-block error-text" ng-message="required">This field is required.</p>
                                <p class="help-block error-text" ng-message="number">This field has to be a number.</p>
                            </div>
                        </div>
                    </div>

                    <div class="form-field" validate-field>
                        <label for="M39">Performance</label>
                        <div class="input-with-unit">
                            <input type="number" name="M39" id="M39" ng-model="calculator.options.fields.M39" required>

                            <div ng-messages="calculatorRoiOptionsForm.M39.$error" role="alert">
                                <p class="help-block error-text" ng-message="required">This field is required.</p>
                                <p class="help-block error-text" ng-message="number">This field has to be a number.</p>
                            </div>
                        </div>
                    </div>

                    <div class="form-field" validate-field>
                        <label for="M51">Quality</label>
                        <div class="input-with-unit">
                            <input type="number" name="M51" id="M51" ng-model="calculator.options.fields.M51" required>

                            <div ng-messages="calculatorRoiOptionsForm.M51.$error" role="alert">
                                <p class="help-block error-text" ng-message="required">This field is required.</p>
                                <p class="help-block error-text" ng-message="number">This field has to be a number.</p>
                            </div>
                        </div>
                    </div>

                    <div class="form-field" validate-field>
                        <label for="M52">Overall OEE</label>
                        <div class="input-with-unit">
                            <input type="number" name="M52" id="M52" ng-model="calculator.options.fields.M52" required>

                            <div ng-messages="calculatorRoiOptionsForm.M52.$error" role="alert">
                                <p class="help-block error-text" ng-message="required">This field is required.</p>
                                <p class="help-block error-text" ng-message="number">This field has to be a number.</p>
                            </div>
                        </div>
                    </div>
                </fieldset>

                <div class="form-footer">
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </form>
        </div>
    <?php
    }
} 