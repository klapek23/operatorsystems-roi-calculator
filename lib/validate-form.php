<?php

require dirname(__FILE__) . '/../vendor/autoload.php';
require_once('./ResultsClass.php');

$request_body = file_get_contents('php://input');
$request_body = json_decode($request_body);

if(isset($request_body) && !empty($request_body)) {

    $data = array(
        'userData' => array(
            'name' => htmlspecialchars($request_body->userData->name),
            'company' => htmlspecialchars($request_body->userData->company),
            'email' => htmlspecialchars($request_body->userData->email)
        ),
        'calculatorConstantValues' => $request_body->calculatorConstantValues,
        'calculatorUserValues' => $request_body->calculatorUserValues,
        'calculations' => $request_body->calculations
    );

    $data['calculations']->M24 = $request_body->M24;

   // var_dump($data['calculatorUserValues']); die();

    //add to database
    $results = new \RoiCalculator\ResultsClass();
    $newResult = new stdClass();
    $newResult->full_name = $data['userData']['name'];
    $newResult->email = $data['userData']['email'];
    $newResult->company = $data['userData']['company'];
    $newResult->calculations = serialize($data['calculations']);

    $dbStatus = $results->createResult($newResult);

    $mail = new PHPMailer;

    //$mail->SMTPDebug = 3;                               // Enable verbose debug output

    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'operatorsyst@gmail.com';                 // SMTP username
    $mail->Password = 'OperatorC2H5OH%40';                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                    // TCP port to connect to

    $mail->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );

    $mail->setFrom('operatorsyst@gmail.com', 'Operatorsystems');
    $mail->addAddress($data['userData']['email'], $data['userData']['name']);     // Add a recipient

    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->CharSet = 'UTF-8';

    $emailContent = '';
    $emailContent .= '<h3>Hello Dear Sir / Madam</h3>';
    $emailContent .= '<p>This email was generated from Operatorsystems.com ROI calculator. Below You can find all calculations:</p>';
    $emailContent .= '<table style="width: 100%; table-layout: fixed; border-collapse: collapse; border: 1px solid #000;">';
    $emailContent .= '<thead>';
    $emailContent .= '<tr>';
    $emailContent .= '<th style="border: 1px solid #000; padding: 5px 10px; text-align: left;"></th>';
    $emailContent .= '<th style="border: 1px solid #000; padding: 5px 10px; text-align: left;">Before improvements</th>';
    $emailContent .= '<th style="border: 1px solid #000; padding: 5px 10px; text-align: left;">After improvements</th>';
    $emailContent .= '</tr>';
    $emailContent .= '</thead>';
    $emailContent .= '<tbody>';

    for($i = 0; $i < count($data['calculations']->beforeImprovements); $i++):
        $emailContent .= '<tr>';
        $emailContent .= '<td style="border: 1px solid #000; padding: 5px 10px; text-align: left;">' . $data["calculations"]->beforeImprovements[$i]->label . '</td>';
        $emailContent .= '<td style="border: 1px solid #000; padding: 5px 10px; text-align: left;">' . $data["calculations"]->beforeImprovements[$i]->value . ' ' . htmlspecialchars($data["calculations"]->beforeImprovements[$i]->unit) . '</td>';
        $emailContent .= '<td style="border: 1px solid #000; padding: 5px 10px; text-align: left;">' . $data["calculations"]->afterImprovements[$i]->value . ' ' . htmlspecialchars($data["calculations"]->afterImprovements[$i]->unit) . '</td>';
        $emailContent .= '</tr>';
    endfor;

    $emailContent .= '</tbody>';
    $emailContent .= '</table>';

    $emailContent .= '<p style="margin: 30px 0;">' . $data["calculations"]->F23->label . ': <strong>' . $data["calculations"]->F23->value . ' ' . $data["calculations"]->F23->unit . '</strong></p>';

    $emailContent .= '<h3>Potential savings:</h3>';
    $emailContent .= '<table style="width: 100%; table-layout: fixed; border-collapse: collapse; border: 1px solid #000;">';
    $emailContent .= '<tbody>';

    for($i = 0; $i < count($data['calculations']->potentialSavings); $i++):
        $emailContent .= '<tr>';
        $emailContent .= '<td style="border: 1px solid #000; padding: 5px 10px; text-align: left;">' . $data["calculations"]->potentialSavings[$i]->label . '</td>';
        $emailContent .= '<td style="border: 1px solid #000; padding: 5px 10px; text-align: left;">' . $data["calculations"]->potentialSavings[$i]->value . ' ' . htmlspecialchars($data["calculations"]->potentialSavings[$i]->unit) . '</td>';
        $emailContent .= '</tr>';
    endfor;

    $emailContent .= '</tbody>';
    $emailContent .= '</table>';

    $emailContent .= '<p style="margin-top: 30px; font-size: 13px;">The following assumptions have been made for the calculation:<br>Average Operator OEE implementation costs for ' . $data['calculatorUserValues']->C10->value . ' identical machines: ' . $data['calculations']->M24->value . ' €.</p>';
    $emailContent .= '<table style="width: 100%; table-layout: fixed; border-collapse: collapse; border: 1px solid #000;">';
    $emailContent .= '<tbody>';

    $emailContent .= '<tr>';
    $emailContent .= '<td style="border: 1px solid #000; padding: 5px 10px; text-align: left;">' . $data['calculatorConstantValues']->C16->label . '</td>';
    $emailContent .= '<td style="border: 1px solid #000; padding: 5px 10px; text-align: left;">' . $data["calculatorConstantValues"]->C16->value . ' ' . htmlspecialchars( $data["calculatorConstantValues"]->C16->unit) . '</td>';
    $emailContent .= '</tr>';

    $emailContent .= '<tr>';
    $emailContent .= '<td style="border: 1px solid #000; padding: 5px 10px; text-align: left;">' . $data['calculatorConstantValues']->C18->label . '</td>';
    $emailContent .= '<td style="border: 1px solid #000; padding: 5px 10px; text-align: left;">' . $data["calculatorConstantValues"]->C18->value . ' ' . htmlspecialchars( $data["calculatorConstantValues"]->C18->unit) . '</td>';
    $emailContent .= '</tr>';

    $emailContent .= '<tr>';
    $emailContent .= '<td style="border: 1px solid #000; padding: 5px 10px; text-align: left;">' . $data['calculatorConstantValues']->C20->label . '</td>';
    $emailContent .= '<td style="border: 1px solid #000; padding: 5px 10px; text-align: left;">' . $data["calculatorConstantValues"]->C20->value . ' ' . htmlspecialchars( $data["calculatorConstantValues"]->C20->unit) . '</td>';
    $emailContent .= '</tr>';

    $emailContent .= '<tr>';
    $emailContent .= '<td style="border: 1px solid #000; padding: 5px 10px; text-align: left;">' . $data['calculatorConstantValues']->C22->label . '</td>';
    $emailContent .= '<td style="border: 1px solid #000; padding: 5px 10px; text-align: left;">' . $data["calculatorConstantValues"]->C22->value . ' ' . htmlspecialchars( $data["calculatorConstantValues"]->C22->unit) . '</td>';
    $emailContent .= '</tr>';

    $emailContent .= '</tbody>';
    $emailContent .= '</table>';

    $mail->Subject = 'ROI calculations and potential savings';
    $mail->Body    = $emailContent;

    if(!$mail->send()) {
        $result = array(
            'success' => false,
            'message' => 'Some error has occurred',
            'error' => $mail->ErrorInfo
        );
    } else {
        //send second email
        $adminEmail = new PHPMailer;

        //$adminEmail->SMTPDebug = 3;

        $adminEmail->isSMTP();                                      // Set mailer to use SMTP
        $adminEmail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
        $adminEmail->SMTPAuth = true;                               // Enable SMTP authentication
        $adminEmail->Username = 'operatorsyst@gmail.com';                 // SMTP username
        $adminEmail->Password = 'OperatorC2H5OH%40';                           // SMTP password
        $adminEmail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $adminEmail->Port = 587;                                    // TCP port to connect to

        $adminEmail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        $adminEmail->setFrom('operatorsyst@gmail.com', 'Operatorsystems');
        $adminEmail->addAddress('piotr.lesniak@operatorsystems.com', 'Piotr Leśniak');     // Add a recipient
        //$adminEmail->addAddress('klapek23@gmail.com', 'Dawid Wojtyca');     // Add a recipient
        //$adminEmail->addAddress('torres23@onet.eu', 'Dawid Wojtyca');     // Add a recipient

        $adminEmail->isHTML(true);                                  // Set email format to HTML
        $adminEmail->CharSet = 'UTF-8';

        $emailContent = '';
        $emailContent .= '<h3>Someone has used ROI Calculator from Operatorsystem website. Below You can find user details:</h3>';
        $emailContent .= '<table style="width: 100%; table-layout: fixed; border-collapse: collapse; border: 1px solid #000; margin-bottom: 30px;">';
        $emailContent .= '<tbody>';

        foreach($data['userData'] as $key => $row):
            $emailContent .= '<tr>';
            $emailContent .= '<td style="border: 1px solid #000; padding: 5px 10px; text-transform: capitalize;">' . $key . '</td>';
            $emailContent .= '<td style="border: 1px solid #000; padding: 5px 10px;"><strong>' . $row . '</strong></td>';
            $emailContent .= '</tr>';
        endforeach;

        $emailContent .= '</tbody>';
        $emailContent .= '</table>';

        $emailContent .= '<table style="width: 100%; table-layout: fixed; border-collapse: collapse; border: 1px solid #000;">';
        $emailContent .= '<thead>';
        $emailContent .= '<tr>';
        $emailContent .= '<th style="border: 1px solid #000; padding: 5px 10px; text-align: left;"></th>';
        $emailContent .= '<th style="border: 1px solid #000; padding: 5px 10px; text-align: left;">Before improvements</th>';
        $emailContent .= '<th style="border: 1px solid #000; padding: 5px 10px; text-align: left;">After improvements</th>';
        $emailContent .= '</tr>';
        $emailContent .= '</thead>';
        $emailContent .= '<tbody>';

        for($i = 0; $i < count($data['calculations']->beforeImprovements); $i++):
            $emailContent .= '<tr>';
            $emailContent .= '<td style="border: 1px solid #000; padding: 5px 10px; text-align: left;">' . $data["calculations"]->beforeImprovements[$i]->label . '</td>';
            $emailContent .= '<td style="border: 1px solid #000; padding: 5px 10px; text-align: left;">' . $data["calculations"]->beforeImprovements[$i]->value . ' ' . htmlspecialchars($data["calculations"]->beforeImprovements[$i]->unit) . '</td>';
            $emailContent .= '<td style="border: 1px solid #000; padding: 5px 10px; text-align: left;">' . $data["calculations"]->afterImprovements[$i]->value . ' ' . htmlspecialchars($data["calculations"]->afterImprovements[$i]->unit) . '</td>';
            $emailContent .= '</tr>';
        endfor;

        $emailContent .= '</tbody>';
        $emailContent .= '</table>';

        $emailContent .= '<h3>Potential savings:</h3>';
        $emailContent .= '<table style="width: 100%; table-layout: fixed; border-collapse: collapse; border: 1px solid #000;">';
        $emailContent .= '<tbody>';

        for($i = 0; $i < count($data['calculations']->potentialSavings); $i++):
            $emailContent .= '<tr>';
            $emailContent .= '<td style="border: 1px solid #000; padding: 5px 10px; text-align: left;">' . $data["calculations"]->potentialSavings[$i]->label . '</td>';
            $emailContent .= '<td style="border: 1px solid #000; padding: 5px 10px; text-align: left;">' . $data["calculations"]->potentialSavings[$i]->value . ' ' . htmlspecialchars($data["calculations"]->potentialSavings[$i]->unit) . '</td>';
            $emailContent .= '</tr>';
        endfor;

        $emailContent .= '</tbody>';
        $emailContent .= '</table>';

        $emailContent .= '<h3>User values:</h3>';
        $emailContent .= '<table style="width: 100%; table-layout: fixed; border-collapse: collapse; border: 1px solid #000;">';
        $emailContent .= '<tbody>';

        foreach($data['calculatorUserValues'] as $key => $value):
            $emailContent .= '<tr>';
            $emailContent .= '<td style="border: 1px solid #000; padding: 5px 10px; text-align: left;">' . $value->label . '</td>';
            $emailContent .= '<td style="border: 1px solid #000; padding: 5px 10px; text-align: left;">' . $value->value . ' ' . htmlspecialchars($value->unit) . '</td>';
            $emailContent .= '</tr>';
        endforeach;

        $emailContent .= '</tbody>';
        $emailContent .= '</table>';


        $emailContent .= '<p style="margin: 30px 0;">' . $data["calculations"]->F23->label . ': <strong>' . $data["calculations"]->F23->value . ' ' . $data["calculations"]->F23->unit . '</strong></p>';

        $adminEmail->Subject = 'ROI Calculator - new calculations';
        $adminEmail->Body    = $emailContent;

        if(!$adminEmail->send()) {
            $result = array(
                'success' => false,
                'message' => 'Some error has occurred',
                'db'    => $dbStatus,
                'error' => $adminEmail->ErrorInfo
            );
        } else {
            $result = array(
                'success' => true,
                'message' => 'Email has been sent',
                'db'    => $dbStatus,
                'error' => null
            );
        }
    }

    echo json_encode($result);
    exit;
}