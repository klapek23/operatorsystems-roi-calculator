<?php

namespace RoiCalculator;

abstract class DbClass {
    protected $resultsTable = 'calculator_roi_results';
    protected $optionsTable = 'calculator_roi_options';

    public function __construct() {
        global $wpdb;
    }
}