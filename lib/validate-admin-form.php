<?php

require_once(dirname(__FILE__) . '/OptionsClass.php');

$request_body = file_get_contents('php://input');
$request_body = json_decode($request_body);

if(isset($request_body) && !empty($request_body)) {

    $optionsAdapter = new \RoiCalculator\OptionsClass();

    if($optionsAdapter->getOptionByName('default_values')) {
        $status = $optionsAdapter->updateOption('default_values', serialize($request_body->calculatorOptions->fields));
    } else {
        $status = $optionsAdapter->createOption('default_values', serialize($request_body->calculatorOptions->fields));
    }

    if(!$status['success']) {
        $result = array(
            'success' => false,
            'message' => __('Some error has occurred', 'calculator-roi'),
            'error' => 'MySQL error'
        );
    } else {
        $result = array(
            'success' => true,
            'message' => __('Options has been saved', 'calculator-roi'),
            'error' => null
        );
    }

    echo json_encode($result);
    exit;
}