<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require_once('../../vendor/autoload.php');
require_once('../../lib/OptionsClass.php');
require_once('../../lib/ResultsClass.php');


$configuration = array(
    'settings' => array(
        'displayErrorDetails' => false
    )
);

$c = new \Slim\Container($configuration);

$app = new \Slim\App($c);

/*$app->add(new \Slim\Middleware\HttpBasicAuthentication(array(
    "secure" => false,
    "users"  => array(
        "operatorsystems"   =>  "b;cxKNm12A1c"
    )
)));*/

$app->get('/options', function (Request $request, Response $response) {
    $options = new \RoiCalculator\OptionsClass();
    $result = $options->getAllOptions();

    for($i = 0; $i < count($result); $i++) {
        if(unserialize($result[$i]['option_value'])) {
            $result[$i]['option_value'] = unserialize($result[$i]['option_value']);
        }
    }

    $jsonResponse = $response->withJson($result);

    return $jsonResponse;
});


$app->get('/options/{param}', function (Request $request, Response $response) {
    $param = $request->getAttribute('param');
    $paramType = filter_var($param, FILTER_VALIDATE_INT);
    $paramType = is_int($paramType) ? 'int' : 'string';

    switch($paramType) {
        case 'int':
            $result = array(
                'type'  =>  'success',
                'msg'   =>  'int'
            );

            break;

        case 'string':
            $options = new \RoiCalculator\OptionsClass();
            $option = $options->getOptionByName($param);

            if($option) {
                if(unserialize($option->option_value)) {
                    $option->option_value = unserialize($option->option_value);
                }
                $result = array(
                    'type'  =>  'success',
                    'data'  =>  $option
                );
            } else {
                $result = array(
                    'type'  =>  'error',
                    'msg'   =>  'No results'
                );
            }

            break;

        default:
            $result = array(
                'type'  =>  'error',
                'msg'   =>  'Failed to check param type'
            );
    }

    $jsonResponse = $response->withJson($result);

    return $jsonResponse;
});


$app->get('/results/{id}', function (Request $request, Response $response) {
    $id = $request->getAttribute('id');

    $results = new \RoiCalculator\ResultsClass();
    $result = $results->getResult($id);

    if(!$result) {
        $result = array(
            'type'  =>  'error',
            'msg'   =>  'Result not found'
        );
    } else {
        $result = array(
            'type'  =>  'success',
            'result' => $result
        );
    }

    $jsonResponse = $response->withJson($result);

    return $jsonResponse;
});


$app->delete('/results/{id}', function (Request $request, Response $response) {
    $id = $request->getAttribute('id');

    $results = new \RoiCalculator\ResultsClass();
    $result = $results->removeResult($id);

    if(!$result) {
        $result = array(
            'type'  =>  'error',
            'msg'   =>  'Result not deleted'
        );
    } else {
        $result = array(
            'type'  =>  'success',
            'msg'   =>  'Result deleted'
        );
    }

    $jsonResponse = $response->withJson($result);

    return $jsonResponse;
});




$app->run();