<?php
/*
Plugin Name: Calculator ROI
Text Domain: calculator-roi
Domain Path: /languages
Description: ROI calculator widget
Version: 1.0
Author: Dawid Wojtyca
*/

namespace RoiCalculator;
//create namespace constant
define(__NAMESPACE__ . '\NS', __NAMESPACE__ . '\\');

define(NS . 'CALCULATOR_ROI_PLUGIN_DIR_URL', plugin_dir_url( __FILE__ ));
define(NS . 'CALCULATOR_ROI_PLUGIN_DIR_PATH', plugin_dir_path( __FILE__ ));

//load plugin textdomain
load_plugin_textdomain( 'calculator-roi', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

//register installation hooks
require_once(CALCULATOR_ROI_PLUGIN_DIR_PATH . 'lib/InstallClass.php');

$install = new InstallClass();
register_activation_hook(   __FILE__, array( $install, 'install' ) );
register_deactivation_hook( __FILE__, array( $install, 'uninstall' ) );


function initPlugin() {
    //if(\is_user_logged_in()) {
        //register public scripts and styles
        require_once(CALCULATOR_ROI_PLUGIN_DIR_PATH . 'lib/PublicClass.php');
        $public = new PublicClass();

        add_action( 'wp_enqueue_scripts', array($public, 'registerStyles') );
        add_action( 'wp_enqueue_scripts', array($public, 'registerScripts') );

        //shortcode init
        function calculatorRoiShortcode() {
            echo '<div id="calculator-roi" class="widget slide-widget calculator-roi-widget" ng-app="CalculatorROI" calculator-roi>
                    <div class="calculator-roi-handle">
                        <i class="fa fa-calculator" aria-hidden="true"></i>
                    </div>

                    <ui-view></ui-view>
                  </div>';
        }

        //add shortcode
        function addShortcode() {
            add_shortcode('calculator-roi', NS . 'calculatorRoiShortcode');
            do_shortcode("[calculator-roi]");
        }

        if (!is_admin()) {
            add_action('wp_footer', NS . 'addShortcode');
        }

        require_once(CALCULATOR_ROI_PLUGIN_DIR_PATH . 'lib/AdminClass.php');
        $adminClass = new AdminClass();

        // add action to display media library users page
        add_action('admin_menu', array($adminClass, 'addMenuItem'));
    //}
}


add_action('init', 'RoiCalculator\initPlugin');

?>