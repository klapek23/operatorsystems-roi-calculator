module.exports = function(grunt) {

    grunt.initConfig({
        jshint: {
            files: ['Gruntfile.js', 'src/**/*.js', 'test/**/*.js'],
            options: {
                globals: {
                    jQuery: true
                }
            }
        },

        copy: {
            viewsapp: {
                expand: true,
                cwd: 'src/',
                src: ['views/**/*.html'],
                dest: 'build/'
            },
            img: {
                expand: true,
                cwd: 'src/',
                src: ['img/**/*.*'],
                dest: 'build/'
            },
            admin_viewsapp: {
                expand: true,
                cwd: 'src/admin/',
                src: ['views/**/*.html'],
                dest: 'build/admin/'
            },
            bootstrapFonts: {
                expand: true,
                cwd: 'bower_components/bootstrap/dist/',
                src: ['fonts/*.*'],
                dest: 'build/'
            },
            jslibs: {
                expand: true,
                cwd: 'bower_components/',
                src: [
                    'angular/angular.min.js',
                    'angular-animate/angular-animate.min.js',
                    'angular-ui-router/release/angular-ui-router.min.js',
                    'angular-messages/angular-messages.min.js',
                    'angular-bootstrap/ui-bootstrap.min.js',
                    'angular-bootstrap/ui-bootstrap-tpls.min.js',
                    'angular-touch/angular-touch.min.js',
                    'Chart.js/dist/Chart.js',
                    'tc-angular-chartjs/dist/tc-angular-chartjs.min.js'
                ],
                dest: 'build/js/libs/'
            },
            adminjslibs: {
                expand: true,
                cwd: 'bower_components/',
                src: [
                    'angular/angular.min.js',
                    'angular-animate/angular-animate.min.js',
                    'angular-messages/angular-messages.min.js',
                    'angular-bootstrap/ui-bootstrap.min.js',
                    'angular-bootstrap/ui-bootstrap-tpls.min.js'
                ],
                dest: 'build/admin/js/libs/'
            }
        },

        less: {
            css: {
                files: {
                    'src/css/calculator-roi.css': 'src/less/calculator-roi.less'
                }
            }
        },

        concat: {
            jsapp: {
                src: [
                    'src/index.js',
                    'src/directives/**/*.js',
                    'src/filters/**/*.js',
                    'src/services/**/*.js',
                    'src/controllers/**/*.js'
                ],
                dest: 'build/js/app.js'
            },
            adminapp: {
                src: [
                    'src/admin/index.js',
                    'src/admin/directives/**/*.js',
                    'src/admin/services/**/*.js',
                    'src/admin/controllers/**/*.js'
                ],
                dest: 'build/admin/js/app.js'
            },
            css: {
                src: [
                    'bower_components/bootstrap/dist/css/bootstrap.min.css',
                    'bower_components/bootstrap/dist/css/bootstrap-theme.min.css',
                    /*'bower_components/angular-chart.js/dist/angular-chart.css',*/
                    'src/css/calculator-roi.css'
                ],
                dest: 'build/css/calculator-roi.css'
            }
        },

        watch: {
            files: ['<%= jshint.files %>'],
            tasks: ['jshint', 'copy', 'concat', 'less']
        }
    });

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['jshint', 'copy', 'less', 'concat']);

};