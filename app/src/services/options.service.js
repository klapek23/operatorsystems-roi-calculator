calculatorApp.factory('OptionsService', function($http) {

    return {
        getOption: function($param) {
            return $http({
                method: 'get',
                /*headers: {
                    'Authorization': 'Basic b3BlcmF0b3JzeXN0ZW1zOmI7Y3hLTm0xMkExYw=='
                },*/
                url: '/wp-content/plugins/calculator-roi/api/v0/api.php/options/' + $param
            }).then(function(response) {
                if(response.data.type === 'error') {
                    if(response.status === 401) {
                        $location.path('/signin');
                        return $q.reject(response);
                    }
                    return false;
                } else {
                    return response.data;
                }
            }, function(err) {
                return err.data;
            });
        },
        getAllOptions: function() {
            return $http({
                method: 'get',
                /*headers: {
                    'Authorization': 'Basic b3BlcmF0b3JzeXN0ZW1zOmI7Y3hLTm0xMkExYw=='
                },*/
                url: '/wp-content/plugins/calculator-roi/api/v0/api.php/options'
            }).then(function(response) {
                return response.data;
            }, function(err) {
                return err.data;
            });
        }
    };
});