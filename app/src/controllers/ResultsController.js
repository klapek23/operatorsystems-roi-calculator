calculatorApp.controller('ResultsController', ['$scope', '$state', '$timeout', function($scope, $state, $timeout) {

    $timeout(function() {
        var ctx = document.getElementById('savings-chart');
        $scope.savingsChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["Availability", "Performance", "Quality", "Overall OEE"],
                datasets: [{
                    label: 'Before',
                    data: [
                        $scope.calculator.calculations.secondStep.F5.value,
                        $scope.calculator.calculations.secondStep.F6.value,
                        $scope.calculator.calculations.secondStep.F7.value,
                        $scope.calculator.calculations.secondStep.F8.value
                    ],
                    backgroundColor: 'rgba(11, 35, 87, .3)',
                    borderColor: 'rgba(11, 35, 87, 1)',
                    borderWidth: 1
                },
                {
                    label: 'After',
                    data: [
                        $scope.calculator.calculations.secondStep.F11.value,
                        $scope.calculator.calculations.secondStep.F12.value,
                        $scope.calculator.calculations.secondStep.F13.value,
                        $scope.calculator.calculations.secondStep.F14.value
                    ],
                    backgroundColor: 'rgba(11, 35, 87, .8)',
                    borderColor: 'rgba(11, 35, 87, 1)',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            callback: function(tick) {
                                return tick + '%';
                            }
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            maxRotation: 0
                        }
                    }]
                },
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + tooltipItem.yLabel + '%';
                        }
                    }
                }
            }
        });
    });

}]);

