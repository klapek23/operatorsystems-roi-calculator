calculatorApp.controller('SavingsChartController', ['$scope', '$state', '$timeout', '$filter', function($scope, $state, $timeout, $filter) {

    $timeout(function() {
        var ctx = document.getElementById('savings-chart');
        $scope.savingsChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["0", "12", "24", "36"],
                datasets: [
                    {
                        type: 'line',
                        label: 'ROI',
                        fill: false,
                        data: [
                            0,
                            $scope.calculator.calculations.secondStep.F28.value,
                            $scope.calculator.calculations.secondStep.F28.value * 2,
                            $scope.calculator.calculations.secondStep.F28.value * 3
                        ],
                        backgroundColor: 'rgba(231, 48, 42, .3)',
                        borderColor: 'rgba(231, 48, 42, 1)',
                        borderWidth: 2
                    },
                    {
                        type: 'bar',
                        label: 'Annual savings',
                        data: [
                            0,
                            $scope.calculator.calculations.secondStep.F26.value,
                            $scope.calculator.calculations.secondStep.F26.value * 2,
                            $scope.calculator.calculations.secondStep.F26.value * 3
                        ],
                        backgroundColor: 'rgba(11, 35, 87, .3)',
                        borderColor: 'rgba(11, 35, 87, 1)',
                        borderWidth: 1
                    }
                ]
            },
            options: {
                responsive: true,
                scales: {
                    yAxes: [{
                        ticks: {
                            callback: function (tick) {
                                return $filter('currency')(tick, '€', 0);
                            }
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            callback: function (tick) {
                                return tick + ' m';
                            }
                        }
                    }]
                },
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + $filter('currency')(tooltipItem.yLabel, '€', 0);
                        }
                    }
                }
            }
        });
    });

}]);

