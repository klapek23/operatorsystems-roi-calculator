calculatorApp.directive('calculatorRoi', ['$timeout', function($timeout) {
    return {
        restrict: 'A',
        link: function (scope, el, attrs) {
            $timeout( function() {
                var handle = angular.element(document.querySelector('.calculator-roi-handle'));

                handle.on('click', function(e) {
                    el.toggleClass('active');
                });
            });
        }
    };
}]);