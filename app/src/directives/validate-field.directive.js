calculatorApp.directive('validateField', ['$timeout', function($timeout) {
    return {
        restrict: 'A',
        link: function (scope, el, attrs) {
            scope.$on('show-errors-check-validity', function(e, form) {
                el.removeClass('show-errors');

                if(scope[form].$invalid) {
                    el.addClass('show-errors');
                }
            });
        }
    };
}]);