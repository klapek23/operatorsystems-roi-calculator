var calculatorApp = angular.module('CalculatorROI', ['ui.router', 'ngAnimate', 'ngMessages', 'ui.bootstrap', 'tc.chartjs'])
    .config(function($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {
        //config
        $urlRouterProvider.otherwise(function($injector, $location) {
            $injector.invoke(['$state', function($state) {
                $state.go('form.page-1');
            }]);
        });

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });

        //$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        //$httpProvider.defaults.headers.common['Authorization'] = 'Basic ' + 'b3BlcmF0b3JzeXN0ZW1zOmI7Y3hLTm0xMkExYw==';

        $stateProvider
            .state('form', {
                templateUrl: "/wp-content/plugins/calculator-roi/app/build/views/form.html",
                controller: 'FormController',
                resolve: {
                    DefaultValues: function(OptionsService) {
                        return OptionsService.getOption('default_values').then(function(response) {
                            if(response.hasOwnProperty('data')) {
                                return response.data.option_value;
                            } else {
                                return false;
                            }
                        }, function(err) {
                            return false;
                        });
                    }
                }
            })
            .state('form.page-1', {
                params: {
                    step: 1
                },
                templateUrl: "/wp-content/plugins/calculator-roi/app/build/views/form.page01.html"
            })
            .state('form.page-2', {
                params: {
                    step:2
                },
                templateUrl: "/wp-content/plugins/calculator-roi/app/build/views/form.page02.html"
            })
            .state('form.page-3', {
                params: {
                    step: 3
                },
                templateUrl: "/wp-content/plugins/calculator-roi/app/build/views/form.page03.html"
            })
            .state('form.page-4', {
                params: {
                    step: 4
                },
                templateUrl: "/wp-content/plugins/calculator-roi/app/build/views/form.results.html",
                controller: 'ResultsController'
            })
            .state('form.page-5', {
                params: {
                    step: 5
                },
                templateUrl: "/wp-content/plugins/calculator-roi/app/build/views/form.savings.html"
            })
            .state('form.page-6', {
                params: {
                    step: 6
                },
                templateUrl: "/wp-content/plugins/calculator-roi/app/build/views/form.savings_chart.html",
                controller: 'SavingsChartController'
            })
            .state('form.page-7', {
                params: {
                    step: 7
                },
                templateUrl: "/wp-content/plugins/calculator-roi/app/build/views/form.signin.html"
            })
            .state('form.finish', {
                params: {
                    step: 8
                },
                templateUrl: "/wp-content/plugins/calculator-roi/app/build/views/form.finish.html"
            });
    })
    .run(function($rootScope) {
        /*$rootScope.$on('$stateChangeStart', function(e, toState, toParams, fromState, fromParams) {
            console.log(toState, toParams);
            console.log(fromState, fromParams);
        });*/
    });

