calculatorAdminApp.controller('ResultsController', ['$scope', '$uibModal', '$http', '$window', function($scope, $uibModal, $http, $window) {

    $scope.init = function(results) {
        $scope.allResults = results;
    };

    $scope.openModal = function(data) {
        var calculationsModal = $uibModal.open({
            controller: ['$scope', '$uibModalInstance', 'data', function($scope, $uibModalInstance, data) {
                $scope.userData = {
                    full_name: data.full_name,
                    email: data.email,
                    company: data.company
                };

                $scope.calculations = data.calculations;

                $scope.closeModal = function() {
                    $uibModalInstance.close();
                };
            }],
            templateUrl: '/wp-content/plugins/calculator-roi/app/build/admin/views/calculations-modal.html',
            resolve: {
                data: data
            },
            windowTopClass: 'wp-modal-popup'
        });
    };

    $scope.removeItem = function(id) {
        if(confirm("Are You sure?") === true) {
            $scope.showLoader = true;

            $http({
                method: 'DELETE',
                headers: {
                    'Authorization': 'Basic b3BlcmF0b3JzeXN0ZW1zOmI7Y3hLTm0xMkExYw'
                },
                url: '/wp-content/plugins/calculator-roi/api/v0/api.php/results/' + id
            }).then(function(response) {
                if(response.data.type !== 'success') {
                    $scope.showLoader = false;
                    $scope.removeError = response.data.message;
                } else {
                    window.location.reload();
                }
            }, function(response) {
                $scope.showLoader = false;
                $scope.removeError = response.data.message;
            });
        }
    };
}]);