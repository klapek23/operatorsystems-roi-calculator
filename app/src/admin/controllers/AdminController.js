calculatorAdminApp.controller('AdminController', ['$scope', '$http', '$uibModal', '$window', function($scope, $http, $uibModal, $window) {

    $scope.calculator = {};
    $scope.calculator.options = {};

    $scope.init = function(defaults) {
        $scope.calculator.options.fields = defaults || {};
    };

    $scope.sendForm = function() {
        $scope.showLoader = true;

        $http({
            method: 'POST',
            url: '/wp-content/plugins/calculator-roi/lib/validate-admin-form.php',
            data: {
                calculatorOptions: $scope.calculator.options
            }
        }).then(function(response) {
            $scope.showLoader = false;
            $scope.formSaveResults = response.data;

            if(!response.data.success) {
                $scope.formSaveError = response.data.message;
            } else {
                angular.element("html, body").animate({ scrollTop: 0 }, "medium");
            }
        }, function(response) {
            $scope.showLoader = false;
            $scope.formSaveResults = response.data;
            $scope.formSaveError = response.data.message;
        });
    };

    $scope.submitForm = function(isValid, $event) {
        if(!isValid) {
            $scope.$broadcast('show-errors-check-validity', 'calculatorRoiOptionsForm');

            $event.preventDefault();
            return false;
        }

        $scope.sendForm();
    };

}]);