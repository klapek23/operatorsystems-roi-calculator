var calculatorApp = angular.module('CalculatorROI', ['ui.router', 'ngAnimate', 'ngMessages', 'ui.bootstrap', 'tc.chartjs'])
    .config(function($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {
        //config
        $urlRouterProvider.otherwise(function($injector, $location) {
            $injector.invoke(['$state', function($state) {
                $state.go('form.page-1');
            }]);
        });

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });

        //$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        //$httpProvider.defaults.headers.common['Authorization'] = 'Basic ' + 'b3BlcmF0b3JzeXN0ZW1zOmI7Y3hLTm0xMkExYw==';

        $stateProvider
            .state('form', {
                templateUrl: "/wp-content/plugins/calculator-roi/app/build/views/form.html",
                controller: 'FormController',
                resolve: {
                    DefaultValues: function(OptionsService) {
                        return OptionsService.getOption('default_values').then(function(response) {
                            if(response.hasOwnProperty('data')) {
                                return response.data.option_value;
                            } else {
                                return false;
                            }
                        }, function(err) {
                            return false;
                        });
                    }
                }
            })
            .state('form.page-1', {
                params: {
                    step: 1
                },
                templateUrl: "/wp-content/plugins/calculator-roi/app/build/views/form.page01.html"
            })
            .state('form.page-2', {
                params: {
                    step:2
                },
                templateUrl: "/wp-content/plugins/calculator-roi/app/build/views/form.page02.html"
            })
            .state('form.page-3', {
                params: {
                    step: 3
                },
                templateUrl: "/wp-content/plugins/calculator-roi/app/build/views/form.page03.html"
            })
            .state('form.page-4', {
                params: {
                    step: 4
                },
                templateUrl: "/wp-content/plugins/calculator-roi/app/build/views/form.results.html",
                controller: 'ResultsController'
            })
            .state('form.page-5', {
                params: {
                    step: 5
                },
                templateUrl: "/wp-content/plugins/calculator-roi/app/build/views/form.savings.html"
            })
            .state('form.page-6', {
                params: {
                    step: 6
                },
                templateUrl: "/wp-content/plugins/calculator-roi/app/build/views/form.savings_chart.html",
                controller: 'SavingsChartController'
            })
            .state('form.page-7', {
                params: {
                    step: 7
                },
                templateUrl: "/wp-content/plugins/calculator-roi/app/build/views/form.signin.html"
            })
            .state('form.finish', {
                params: {
                    step: 8
                },
                templateUrl: "/wp-content/plugins/calculator-roi/app/build/views/form.finish.html"
            });
    })
    .run(function($rootScope) {
        /*$rootScope.$on('$stateChangeStart', function(e, toState, toParams, fromState, fromParams) {
            console.log(toState, toParams);
            console.log(fromState, fromParams);
        });*/
    });


calculatorApp.directive('calculatorRoi', ['$timeout', function($timeout) {
    return {
        restrict: 'A',
        link: function (scope, el, attrs) {
            $timeout( function() {
                var handle = angular.element(document.querySelector('.calculator-roi-handle'));

                handle.on('click', function(e) {
                    el.toggleClass('active');
                });
            });
        }
    };
}]);
calculatorApp.directive('validateField', ['$timeout', function($timeout) {
    return {
        restrict: 'A',
        link: function (scope, el, attrs) {
            scope.$on('show-errors-check-validity', function(e, form) {
                el.removeClass('show-errors');

                if(scope[form].$invalid) {
                    el.addClass('show-errors');
                }
            });
        }
    };
}]);
calculatorApp.factory('OptionsService', function($http) {

    return {
        getOption: function($param) {
            return $http({
                method: 'get',
                /*headers: {
                    'Authorization': 'Basic b3BlcmF0b3JzeXN0ZW1zOmI7Y3hLTm0xMkExYw=='
                },*/
                url: '/wp-content/plugins/calculator-roi/api/v0/api.php/options/' + $param
            }).then(function(response) {
                if(response.data.type === 'error') {
                    if(response.status === 401) {
                        $location.path('/signin');
                        return $q.reject(response);
                    }
                    return false;
                } else {
                    return response.data;
                }
            }, function(err) {
                return err.data;
            });
        },
        getAllOptions: function() {
            return $http({
                method: 'get',
                /*headers: {
                    'Authorization': 'Basic b3BlcmF0b3JzeXN0ZW1zOmI7Y3hLTm0xMkExYw=='
                },*/
                url: '/wp-content/plugins/calculator-roi/api/v0/api.php/options'
            }).then(function(response) {
                return response.data;
            }, function(err) {
                return err.data;
            });
        }
    };
});
/**
 * Created by klapek on 2016-06-19.
 */

calculatorApp.controller('FormController', ['$scope', '$state', '$rootScope', '$http', 'DefaultValues', function($scope, $state, $rootScope, $http, DefaultValues) {

    $scope.currentStep = parseInt($state.params.step, 10);
    $scope.pageType = $state.params.pageType;
    $scope.userData = {};
    $scope.showLoader = false;
    $scope.defaultValues = {};

    $scope.defaultValues.C16 = DefaultValues.C16 || 52;
    $scope.defaultValues.C18 = DefaultValues.C18 || 7;
    $scope.defaultValues.C20 = DefaultValues.C20 || 3;
    $scope.defaultValues.C22 = DefaultValues.C22 || 8;
    $scope.defaultValues.M19 = DefaultValues.M19 || 6720;
    $scope.defaultValues.M20 = DefaultValues.M20 || 10000;
    $scope.defaultValues.M21 = DefaultValues.M21 || 1000;
    $scope.defaultValues.M25 = DefaultValues.M25 || 21;
    $scope.defaultValues.M28 = DefaultValues.M28 || 168;
    $scope.defaultValues.M29 = DefaultValues.M29 || 5;
    $scope.defaultValues.M40 = DefaultValues.M40 || 30;
    $scope.defaultValues.M41 = DefaultValues.M41 || 30;
    $scope.defaultValues.M49 = DefaultValues.M49 || 90;
    $scope.defaultValues.M50 = DefaultValues.M50 || 95;
    $scope.defaultValues.M51 = DefaultValues.M51 || 99.9;
    $scope.defaultValues.M52 = DefaultValues.M52 || 85;

    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
        $scope.currentStep = parseInt(toState.params.step, 10);
    });

    $scope.calculator = {};
    $scope.calculator.calculations = {};
    $scope.calculator.userValues = {
        //# of identical machines
        C10: {
            label: '# of identical machines',
            value: null
        },
        //Direct machine costs / hour (in avg.)
        C12: {
            label: 'Direct machine costs / hour (in avg.)',
            value: null
        },
        //Indirect machine costs - i.e. overhead such as power, etc. (in %)
        C14: {
            label: 'Indirect machine costs - i.e. overhead such as power, etc. (in %)',
            value: null
        },
        //Installed capacity for a machine / shift
        C24: {
            label: 'Installed capacity for a machine / shift',
            value: null
        },
        //Current # of pieces produced by a machine / shift
        C26: {
            label: 'Current # of pieces produced by a machine / shift',
            value: null
        },
        //Target # of downtime minutes for a machine / shift
        C31: {
            label: 'Target # of downtime minutes for a machine / shift',
            value: null
        },
        //Current # of rejected or scraped pieces at machine / shift
        C35: {
            label: 'Current # of rejected or scraped pieces at machine / shift',
            value: null
        },
        //Target # of rejected or scraped pieces at machine / shift
        C37: {
            label: 'Target # of rejected or scraped pieces at machine / shift',
            value: null
        },
        //# of months required to payback the investment in OEE tracking
        C40: {
            label: '# of months required to payback the investment in OEE tracking',
            value: null
        },
        //Expected # of months to meet the target improvements
        C42: {
            label: 'Expected # of months to meet the target improvements ',
            value: null
        }
    };
    $scope.calculator.constantValues = {

        /* ---------- C ---------- */

        //# of weeks / year the identical machines are in operation
        C16: {
            label: '# of weeks / year the identical machines are in operation',
            value: $scope.defaultValues.C16
        },
        //# of days / week the identical machines are in operation
        C18: {
            label: '# of days / week the identical machines are in operation',
            value: $scope.defaultValues.C18
        },
        //# of shifts / day the identical machines are in operation
        C20: {
            label: '# of shifts / day the identical machines are in operation',
            value: $scope.defaultValues.C20
        },
        //# of hours / shift the identical machines are in operation (including all breaks)
        C22: {
            label: '# of hours / shift the identical machines are in operation (including all breaks)',
            value: $scope.defaultValues.C22
        },

        /* ---------- M ---------- */

        //Basic costs (foundation) € / os
        M19: {
            label: 'Basic costs (foundation) €',
            value: $scope.defaultValues.M19
        },
        //Implementations € / os
        M20: {
            label: 'Implementations € / os',
            value: $scope.defaultValues.M20
        },
        //License costs per machine € / os
        M21: {
            label: 'License costs per machine €',
            value: $scope.defaultValues.M21
        },
        //Annual maintenance costs (in % / os)
        M25: {
            label: 'Annual maintenance costs (in %)',
            value: $scope.defaultValues.M25,
            realValue: $scope.defaultValues.M25 / 100
        },
        //Annual maintenance costs (in % / os)
        M28: {
            label: 'Machine rolout 1 hour consultancy cost',
            value: $scope.defaultValues.M28,
            unit: '€'
        },
        //Annual maintenance costs (in % / os)
        M29: {
            label: 'Machine rolout asistance hour qty',
            value: $scope.defaultValues.M29
        },
        //Meal Break (pre-defined, as 1@30 min.)
        M40: {
            label: 'Meal Break (pre-defined, as 1@30 min.)',
            value: $scope.defaultValues.M40
        },
        //Short Breaks (pre-defined, as 2@15 min.)
        M41: {
            label: 'Short Breaks (pre-defined, as 2@15 min.)',
            value: $scope.defaultValues.M41
        },

        /* ---------- World Class OEE ---------- */

        //Availability %
        M49: {
            label: 'Availability',
            value: $scope.defaultValues.M49
        },
        //Performance %
        M50: {
            label: 'Performance',
            value: $scope.defaultValues.M50
        },
        //Quality %
        M51: {
            label: 'Quality',
            value: $scope.defaultValues.M51
        },
        //Overall OEE %
        M52: {
            label: 'Overall OEE',
            value: $scope.defaultValues.M52
        }
    };

    function calculateValues() {
        var calculations = {};

        switch($scope.currentStep) {
            case 1:
                /* ---------- FIRST STEP ---------- */
                calculations.firstStep = (function() {
                    var calculations = {};

                    //Shift length (min.)
                    calculations.M39 = {
                        label: 'Shift length (min.)',
                        value: $scope.calculator.constantValues.C22.value * 60
                    };

                    /* ---------- Current (Actual) Downtime Calculation ---------- */

                    //Shift (min.)
                    calculations.M55 = {
                        label: 'Shift (min.)',
                        value: calculations.M39.value
                    };

                    //Meal break
                    calculations.M56 = {
                        label: 'Meal break',
                        value: $scope.calculator.constantValues.M40.value
                    };

                    //Short break
                    calculations.M57 = {
                        label: 'Short break',
                        value: $scope.calculator.constantValues.M41.value
                    };

                    //Shift after planned breaks (min.)
                    calculations.M58 = {
                        label: 'Shift after planned breaks (min.)',
                        value: calculations.M55.value - calculations.M56.value - calculations.M57.value
                    };

                    //Currently the time that takes to produce a piece (min.)
                    calculations.M59 = {
                        label: 'Currently the time that takes to produce a piece (min.)',
                        value: (calculations.M58.value / $scope.calculator.userValues.C26.value).toFixed(2),
                        realValue: (calculations.M58.value / $scope.calculator.userValues.C26.value)
                    };

                    //Ideally should the time take to produce a piece (min.)
                    calculations.M60 = {
                        label: 'Ideally should the time take to produce a piece (min.)',
                        value: (calculations.M58.value / $scope.calculator.userValues.C24.value).toFixed(2),
                        realValue: calculations.M58.value / $scope.calculator.userValues.C24.value
                    };

                    //More minutes used to produce a pices (currently)
                    calculations.M61 = {
                        label: 'More minutes used to produce a pices (currently)',
                        value: (calculations.M59.realValue - calculations.M60.realValue).toFixed(2),
                        realValue: calculations.M59.realValue - calculations.M60.realValue
                    };

                    //Downtime / shift
                    calculations.M62 = {
                        label: 'Downtime / shift',
                        value: Math.round(calculations.M61.realValue * $scope.calculator.userValues.C26.value),
                        realValue: calculations.M61.realValue * $scope.calculator.userValues.C26.value
                    };

                    //Rolout per machine cost
                    calculations.M30 = {
                        label: 'Rolout per machine cost',
                        value: $scope.calculator.constantValues.M28.value * $scope.calculator.constantValues.M29.value,
                        unit: '€'
                    };

                    //Operator OEE basic cost
                    calculations.M32 = {
                        label: 'Operator OEE basic cost',
                        value: $scope.calculator.constantValues.M19.value + $scope.calculator.constantValues.M20.value,
                        unit: '€'
                    };

                    //Operator OEE rolout for all machines cost
                    calculations.M33 = {
                        label: 'Operator OEE rolout for all machines cost',
                        value: ($scope.calculator.constantValues.M21.value + calculations.M30.value) * $scope.calculator.userValues.C10.value,
                        unit: '€'
                    };

                    //Full Operator implementation cost
                    calculations.M34 = {
                        label: 'Full Operator implementation cost',
                        value: calculations.M32.value + calculations.M33.value,
                        unit: '€'
                    };

                    /* ---------- Displayed calculations ----------- */
                    calculations.display = [calculations.M62];

                    return calculations;
                })();

                //angular.extend(calculations, calculations.firstStep);

                /* ---------- FIRST STEP ---------- */
                break;
            case 2:
                /* ---------- SECOND STEP ---------- */
                calculations.secondStep = (function() {
                    var calculations = {};

                    //Current # of pieces produced by all machines / shift
                    calculations.M4 = {
                        label: 'Current # of pieces produced by all machines / shift',
                        value: $scope.calculator.userValues.C26.value * $scope.calculator.userValues.C10.value
                    };

                    //Current # of rejected or scraped pieces at all machines / shift
                    calculations.M5 = {
                        label: 'Target # of downtime minutes for a machine / shift',
                        value: $scope.calculator.userValues.C35.value * $scope.calculator.userValues.C10.value
                    };

                    //Installed capacity for all machines / shift
                    calculations.M6 = {
                        label: 'Installed capacity for all machines / shift',
                        value: $scope.calculator.userValues.C24.value * $scope.calculator.userValues.C10.value
                    };

                    //Good Pieces
                    calculations.M7 = {
                        label: 'Good Pieces',
                        value: calculations.M4.value - calculations.M5.value
                    };

                    //Target # of Rejected or Scraped Pieces / shift
                    calculations.M8 = {
                        label: 'Target # of Rejected or Scraped Pieces / shift',
                        value: $scope.calculator.userValues.C37.value * $scope.calculator.userValues.C10.value
                    };

                    //# of Hours / Week Machine Operates
                    calculations.M10 = {
                        label: '# of Hours / Week Machine Operates',
                        value: $scope.calculator.constantValues.C22.value * $scope.calculator.constantValues.C20.value * $scope.calculator.constantValues.C18.value
                    };

                    //Direct machine costs / Week €
                    calculations.M11 = {
                        label: 'Direct machine costs / Week',
                        value: $scope.calculator.userValues.C12.value * calculations.M10.value
                    };

                    //Direct machine costs / year €
                    calculations.M12 = {
                        label: 'Direct machine costs / year',
                        value: $scope.calculator.constantValues.C16.value * calculations.M11.value
                    };

                    //Total machine costs / year (here incl. indirect costs) €
                    calculations.M13 = {
                        label: 'Total machine costs / year (here incl. indirect costs)',
                        value: calculations.M12.value * (1 + ($scope.calculator.userValues.C14.value * 0.01))
                    };

                    //Total costs for all machines / Year €
                    calculations.M15 = {
                        label: 'Total costs for all machines / Year',
                        value: calculations.M13.value * $scope.calculator.userValues.C10.value
                    };

                    //Costs of defects and quality losses (e.g. scrap, rework, manipulation, etc.) €
                    calculations.M16 = {
                        label: 'Costs of defects and quality losses (e.g. scrap, rework, manipulation, etc.)',
                        value: Math.round(((calculations.M15.value * calculations.M5.value) / (calculations.M4.value - calculations.M5.value)))
                    };

                    //Total operational costs at the plant €
                    calculations.M17 = {
                        label: 'Total operational costs at the plant',
                        value: Math.round(calculations.M15.value + calculations.M16.value)
                    };

                    //License costs for all machines €
                    calculations.M22 = {
                        label: 'License costs for all machines',
                        value: $scope.calculator.constantValues.M21.value * $scope.calculator.userValues.C10.value
                    };

                    //Total investments in OEE tracking for all machines €
                    calculations.M23 = {
                        label: 'Total investments in OEE tracking for all machines',
                        value: calculations.M22.value + $scope.calculator.constantValues.M19.value + $scope.calculator.constantValues.M20.value
                    };

                    //Annual maintenance costs for OEE tracking €
                    calculations.M26 = {
                        label: 'Annual maintenance costs for OEE tracking €',
                        value: Math.round($scope.calculator.constantValues.M25.realValue * (calculations.M22.value + $scope.calculator.constantValues.M19.value))
                    };

                    //Total investments in OEE tracking for all machines, incl. yearly maintenance  €
                    calculations.M24 = {
                        label: 'Total investments in OEE tracking for all machines, incl. yearly maintenance',
                        value: calculations.M23.value + calculations.M26.value
                    };

                    //Monthly maintenace cost
                    calculations.M31 = {
                        label: 'Monthly maintenace cost',
                        value: calculations.M26.value / 12,
                        unit: '€'
                    };

                    //Full Operator implementation cost incl. maintenance
                    calculations.M35 = {
                        label: 'Full Operator implementation cost incl. maintenance',
                        value: $scope.calculator.calculations.firstStep.M34.value + (($scope.calculator.userValues.C40.value - $scope.calculator.userValues.C42.value) * calculations.M31.value),
                        unit: '€'
                    };

                    //Planned production time (min.)
                    calculations.M42 = {
                        label: 'Planned production time (min.)',
                        value: ($scope.calculator.calculations.firstStep.M39.value - $scope.calculator.constantValues.M40.value - $scope.calculator.constantValues.M41.value) * $scope.calculator.userValues.C10.value
                    };

                    //Operating time (min.)
                    calculations.M43 = {
                        label: 'Operating time (min.)',
                        value: calculations.M42.value - ($scope.calculator.calculations.firstStep.M62.realValue * $scope.calculator.userValues.C10.value)
                    };

                    //Ideal run rate
                    calculations.M44 = {
                        label: 'Ideal run rate',
                        value: calculations.M6.value / calculations.M43.value
                    };

                    //Target Operating time (min.)
                    calculations.M45 = {
                        label: 'Target Operating time (min.)',
                        value: calculations.M42.value - ($scope.calculator.userValues.C31.value * $scope.calculator.userValues.C10.value)
                    };

                    //Optimized Operating Time (min.)
                    calculations.M46 = {
                        label: 'Optimized Operating Time (min.)',
                        value: calculations.M43.value - (calculations.M45.value - calculations.M43.value)
                    };


                    /* ---------- Business Case Report ----------- */
                    calculations.F5 = {
                        label: 'Availability',
                        value: Math.round(((calculations.M43.value / calculations.M42.value) * 100) * 10) / 10,
                        realValue: calculations.M43.value / calculations.M42.value,
                        unit: '%'
                    };

                    calculations.F6 = {
                        label: 'Performance',
                        value: Math.round((((calculations.M4.value / calculations.M43.value) / calculations.M44.value) * 100) * 10) / 10,
                        realValue:  (calculations.M4.value / calculations.M43.value) / calculations.M44.value,
                        unit: '%'
                    };

                    calculations.F7 = {
                        label: 'Quality',
                        value: Math.round(((calculations.M7.value / calculations.M4.value) * 100) * 10) / 10,
                        realValue: calculations.M7.value / calculations.M4.value,
                        unit: '%'
                    };

                    calculations.F8 = {
                        label: 'Overall OEE',
                        value: Math.round(((calculations.F5.realValue * calculations.F6.realValue * calculations.F7.realValue) * 100) * 10) / 10,
                        realValue: calculations.F5.realValue * calculations.F6.realValue * calculations.F7.realValue,
                        unit: '%'
                    };

                    calculations.F11 = {
                        label: 'Availability',
                        value: Math.round(((calculations.M45.value / calculations.M42.value) * 100) * 10) / 10,
                        realValue: calculations.M45.value / calculations.M42.value,
                        unit: '%'
                    };

                    calculations.F12 = {
                        label: 'Performance',
                        value: Math.round((((calculations.M4.value / calculations.M46.value) / calculations.M44.value) * 100) * 10) / 10,
                        realValue: (calculations.M4.value / calculations.M46.value) / calculations.M44.value,
                        unit: '%'
                    };

                    calculations.F13 = {
                        label: 'Quality',
                        value: Math.round((((calculations.M4.value - calculations.M8.value) / calculations.M4.value) * 100) * 10) / 10,
                        realValue: (calculations.M4.value - calculations.M8.value) / calculations.M4.value,
                        unit: '%'
                    };

                    calculations.F14 = {
                        label: 'Overall OEE (target)',
                        value: Math.round(((calculations.F11.realValue * calculations.F12.realValue * calculations.F13.realValue) * 100) * 10) / 10,
                        realValue: calculations.F11.realValue * calculations.F12.realValue * calculations.F13.realValue,
                        unit: '%'
                    };

                    calculations.F23 = {
                        label: 'Potential for more output & capacity after improvements in OEE',
                        value: Math.round(((calculations.F14.realValue / calculations.F8.realValue - 1) * 100) * 10) / 10,
                        realValue: calculations.F14.realValue / calculations.F8.realValue - 1,
                        unit: '%'
                    };

                    calculations.F26 = {
                        label: 'Annual savings on total machine costs after OEE improvements',
                        value: Math.round(calculations.M15.value * (1 - calculations.F8.realValue / calculations.F14.realValue)),
                        unit: '€',
                        tooltip: 'Annual savings if the same quantities are produced but quicker. The costs of defects and quality losses are excluded in this calculation'
                    };

                    calculations.F27 = {
                        label: 'Annual savings after recovering defects and quality losses',
                        value: Math.round(calculations.M16.value * (calculations.F13.realValue - calculations.F7.realValue) / (1 - calculations.F7.realValue)),
                        unit: '€',
                        tooltip: 'Annual savings by additional quality improvements'
                    };

                    calculations.F29 = {
                        label: 'Total savings in the required payback period',
                        value: Math.round((calculations.F27.value + calculations.F26.value) - calculations.M35.value),
                        realValue: (calculations.F27.value + calculations.F26.value) - calculations.M35.value,
                        unit: '€',
                        tooltip: 'Period for target improvements are subtraced from required paybck time, since the effect of improvements are assumed to happen then'
                    };

                    calculations.F28 = {
                        label: 'Total annual savings after OEE tracking costs',
                        value: Math.round(calculations.F29.value / ($scope.calculator.userValues.C40.value - $scope.calculator.userValues.C42.value) * 12),
                        unit: '€'
                    };

                    calculations.F30 = {
                        label: 'Return on OEE Investment (ROI)',
                        value: Math.round(((calculations.F29.realValue - calculations.M35.value) / calculations.M35.value) * 1000) / 10,
                        unit: '%'
                    };

                    calculations.F31 = {
                        label: 'Actual payback time (in months)',
                        value: Math.round(((calculations.M35.value / calculations.F29.value) + $scope.calculator.userValues.C42.value) * 10) / 10,
                        unit: '',
                        tooltip: 'This indicates how fast the OEE investment can be covered after all the improvements are implemented'
                    };

                    calculations.F32 = {
                        label: '',
                        value: Math.round((((($scope.calculator.userValues.C40.value - $scope.calculator.userValues.C42.value) * calculations.M35.value) / calculations.F29.realValue) + $scope.calculator.userValues.C42.value) * 10) / 10,
                        unit: ''
                    };

                    calculations.F33 = {
                        label: 'Required increase in OEE to breakeven the investment (in % points)',
                        value: Math.round((((calculations.M35.value* calculations.F23.realValue) / calculations.F29.realValue) * 100) * 10) / 10,
                        realValue: (calculations.M23.value * calculations.F23.realValue) / calculations.F29.realValue,
                        unit: '%'
                    };

                    calculations.F34 = {
                        label: 'Breakeven OEE',
                        value: Math.round(((calculations.F8.realValue * (1 + calculations.F33.realValue)) * 100) * 10) / 10,
                        realValue: calculations.F8.realValue * (1 + calculations.F33.realValue),
                        unit: '%',
                        tooltip: 'This indicates the minimum required level of OEE to pay for the investments '
                    };

                    var beforeImprovements = [calculations.F5, calculations.F6, calculations.F7, calculations.F8];
                    var afterImprovements = [calculations.F11, calculations.F12, calculations.F13, calculations.F14];


                    /* ---------- Displayed calculations ----------- */
                    calculations.display = {
                        beforeImprovements: beforeImprovements,
                        afterImprovements: afterImprovements,
                        F23: calculations.F23,
                        potentialSavings: [
                            calculations.F26,
                            calculations.F28,
                            calculations.F29,
                            calculations.F31
                        ]
                    };

                    return calculations;
                })();

                //angular.extend(calculations, calculations.secondStep);

                /* ---------- SECOND STEP ---------- */
                break;
        }

        return calculations;
    }

    $scope.changeFormStep = function(action, isValid, $event) {
        if(action === 'forward') {
            if(!isValid) {
                $scope.$broadcast('show-errors-check-validity', 'calculatorRoiForm');

                $event.preventDefault();
                return false;
            }

            var newCalculations = calculateValues();
            angular.extend($scope.calculator.calculations, newCalculations);

            $state.go('form.page-' + ($scope.currentStep + 1));
        } else {
            $state.go('form.page-' + ($scope.currentStep - 1));
        }
    };

    $scope.sendForm = function(isValid, $event) {
        if(!isValid) {
            $scope.$broadcast('show-errors-check-validity', 'calculatorRoiForm');

            $event.preventDefault();
            return false;
        }

        $scope.showLoader = true;
        $scope.singinFormError = false;

        $http({
            method: 'POST',
            url: '/wp-content/plugins/calculator-roi/lib/validate-form.php',
            data: {
                userData: {
                    name: $scope.userData.name,
                    company: $scope.userData.company,
                    email: $scope.userData.email
                },
                calculatorConstantValues: $scope.calculator.constantValues,
                calculatorUserValues: $scope.calculator.userValues,
                calculations: $scope.calculator.calculations.secondStep.display,
                M24: $scope.calculator.calculations.secondStep.M24
            }
        }).then(function(response) {
            $scope.showLoader = false;
            $scope.sendEmailResults = response.data;

            if(!response.data.success) {
                $scope.singinFormError = response.data.message;
            } else {
                $state.go('form.finish');
            }
        }, function(response) {
            $scope.showLoader = false;
            $scope.sendEmailResults = response.data;
            $scope.singinFormError = response.data.message;
        });
    };
}]);
calculatorApp.controller('ResultsController', ['$scope', '$state', '$timeout', function($scope, $state, $timeout) {

    $timeout(function() {
        var ctx = document.getElementById('savings-chart');
        $scope.savingsChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["Availability", "Performance", "Quality", "Overall OEE"],
                datasets: [{
                    label: 'Before',
                    data: [
                        $scope.calculator.calculations.secondStep.F5.value,
                        $scope.calculator.calculations.secondStep.F6.value,
                        $scope.calculator.calculations.secondStep.F7.value,
                        $scope.calculator.calculations.secondStep.F8.value
                    ],
                    backgroundColor: 'rgba(11, 35, 87, .3)',
                    borderColor: 'rgba(11, 35, 87, 1)',
                    borderWidth: 1
                },
                {
                    label: 'After',
                    data: [
                        $scope.calculator.calculations.secondStep.F11.value,
                        $scope.calculator.calculations.secondStep.F12.value,
                        $scope.calculator.calculations.secondStep.F13.value,
                        $scope.calculator.calculations.secondStep.F14.value
                    ],
                    backgroundColor: 'rgba(11, 35, 87, .8)',
                    borderColor: 'rgba(11, 35, 87, 1)',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            callback: function(tick) {
                                return tick + '%';
                            }
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            maxRotation: 0
                        }
                    }]
                },
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + tooltipItem.yLabel + '%';
                        }
                    }
                }
            }
        });
    });

}]);


calculatorApp.controller('SavingsChartController', ['$scope', '$state', '$timeout', '$filter', function($scope, $state, $timeout, $filter) {

    $timeout(function() {
        var ctx = document.getElementById('savings-chart');
        $scope.savingsChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["0", "12", "24", "36"],
                datasets: [
                    {
                        type: 'line',
                        label: 'ROI',
                        fill: false,
                        data: [
                            0,
                            $scope.calculator.calculations.secondStep.F28.value,
                            $scope.calculator.calculations.secondStep.F28.value * 2,
                            $scope.calculator.calculations.secondStep.F28.value * 3
                        ],
                        backgroundColor: 'rgba(231, 48, 42, .3)',
                        borderColor: 'rgba(231, 48, 42, 1)',
                        borderWidth: 2
                    },
                    {
                        type: 'bar',
                        label: 'Annual savings',
                        data: [
                            0,
                            $scope.calculator.calculations.secondStep.F26.value,
                            $scope.calculator.calculations.secondStep.F26.value * 2,
                            $scope.calculator.calculations.secondStep.F26.value * 3
                        ],
                        backgroundColor: 'rgba(11, 35, 87, .3)',
                        borderColor: 'rgba(11, 35, 87, 1)',
                        borderWidth: 1
                    }
                ]
            },
            options: {
                responsive: true,
                scales: {
                    yAxes: [{
                        ticks: {
                            callback: function (tick) {
                                return $filter('currency')(tick, '€', 0);
                            }
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            callback: function (tick) {
                                return tick + ' m';
                            }
                        }
                    }]
                },
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + $filter('currency')(tooltipItem.yLabel, '€', 0);
                        }
                    }
                }
            }
        });
    });

}]);

