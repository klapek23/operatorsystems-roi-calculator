var calculatorAdminApp = angular.module('CalculatorROIAdmin', ['ngAnimate', 'ngMessages', 'ui.bootstrap']);


calculatorAdminApp.directive('validateField', ['$timeout', function($timeout) {
    return {
        restrict: 'A',
        link: function (scope, el, attrs) {
            scope.$on('show-errors-check-validity', function(e, form) {
                console.log(form);
                el.removeClass('show-errors');

                if(scope[form].$invalid) {
                    el.addClass('show-errors');
                }
            });
        }
    };
}]);
calculatorAdminApp.controller('AdminController', ['$scope', '$http', '$uibModal', '$window', function($scope, $http, $uibModal, $window) {

    $scope.calculator = {};
    $scope.calculator.options = {};

    $scope.init = function(defaults) {
        $scope.calculator.options.fields = defaults || {};
    };

    $scope.sendForm = function() {
        $scope.showLoader = true;

        $http({
            method: 'POST',
            url: '/wp-content/plugins/calculator-roi/lib/validate-admin-form.php',
            data: {
                calculatorOptions: $scope.calculator.options
            }
        }).then(function(response) {
            $scope.showLoader = false;
            $scope.formSaveResults = response.data;

            if(!response.data.success) {
                $scope.formSaveError = response.data.message;
            } else {
                angular.element("html, body").animate({ scrollTop: 0 }, "medium");
            }
        }, function(response) {
            $scope.showLoader = false;
            $scope.formSaveResults = response.data;
            $scope.formSaveError = response.data.message;
        });
    };

    $scope.submitForm = function(isValid, $event) {
        if(!isValid) {
            $scope.$broadcast('show-errors-check-validity', 'calculatorRoiOptionsForm');

            $event.preventDefault();
            return false;
        }

        $scope.sendForm();
    };

}]);
calculatorAdminApp.controller('ResultsController', ['$scope', '$uibModal', '$http', '$window', function($scope, $uibModal, $http, $window) {

    $scope.init = function(results) {
        $scope.allResults = results;
    };

    $scope.openModal = function(data) {
        var calculationsModal = $uibModal.open({
            controller: ['$scope', '$uibModalInstance', 'data', function($scope, $uibModalInstance, data) {
                $scope.userData = {
                    full_name: data.full_name,
                    email: data.email,
                    company: data.company
                };

                $scope.calculations = data.calculations;

                $scope.closeModal = function() {
                    $uibModalInstance.close();
                };
            }],
            templateUrl: '/wp-content/plugins/calculator-roi/app/build/admin/views/calculations-modal.html',
            resolve: {
                data: data
            },
            windowTopClass: 'wp-modal-popup'
        });
    };

    $scope.removeItem = function(id) {
        if(confirm("Are You sure?") === true) {
            $scope.showLoader = true;

            $http({
                method: 'DELETE',
                headers: {
                    'Authorization': 'Basic b3BlcmF0b3JzeXN0ZW1zOmI7Y3hLTm0xMkExYw'
                },
                url: '/wp-content/plugins/calculator-roi/api/v0/api.php/results/' + id
            }).then(function(response) {
                if(response.data.type !== 'success') {
                    $scope.showLoader = false;
                    $scope.removeError = response.data.message;
                } else {
                    window.location.reload();
                }
            }, function(response) {
                $scope.showLoader = false;
                $scope.removeError = response.data.message;
            });
        }
    };
}]);